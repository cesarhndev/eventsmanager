<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_EventRegister extends Mage_Core_Model_Abstract
{
    private $eventService;
    private $typeService;
    private $typeRepository;

    public function __construct()
    {
        $this->eventService = Mage::getModel('onestic_eventsmanager/service_eventService');
        $this->typeService = Mage::getModel('onestic_eventsmanager/service_typeService');
        $this->typeRepository = Mage::getModel('onestic_eventsmanager/repository_types');
    }

    /*
     * Register an event
     *
     * @param string $description
     * @param string $additional_info
     * @param string $type
     */
    public function registerEvent($description, $additional_info, $type = null){

        $typeId = $this->getTypeId($type);
        $currentDate = $this->getCurrentDate();

        return $this->eventService->createEvent($description, $additional_info, $currentDate, $typeId);
    }

    /*
     * Get current system date
     *
     * @return string
     */
    private function getCurrentDate(){
        return strtotime(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
    }

    /*
     * Get Type Id from label
     *
     * @param string $typeLabel
     * @return int $typeId
     */
    private function getTypeIdFromLabel($typeLabel){
        $typeId = $this->typeRepository->getTypeIdByLabel($typeLabel);

        if(!$typeId){
            $typeId = $this->typeService->createTypeByLabel($typeLabel);
        }

        return $typeId;
    }

    /*
    * Get Type Id
    *
    * @param string $type
    * @return int $typeId
    */
    private function getTypeId($type){

        if(strlen($type)){
            $typeId = $this->getTypeIdFromLabel($type);
        } else {
            $typeId = $this->typeRepository->getDefaultTypeId();
        }

        return $typeId;
    }
}