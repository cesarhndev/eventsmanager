<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_Repository_Events extends Mage_Core_Model_Abstract
{
    private $eventsModel;
    private $typesArray;

    protected function _construct()
    {
        $this->eventsModel = Mage::getModel('onestic_eventsmanager/events');
        $this->typesArray = Mage::getModel('onestic_eventsmanager/repository_types')->getOptionArray();
    }

    /*
     * Get events to notify. Must be filter by type and date
     *
     * @param int $type
     * @param string $dateToFilter
     * @return $collection
     */
    public function getEventsCollectionTypeToNotify($type, $dateToFilter){

        return $this->eventsModel->getCollection()
            ->addFieldToFilter('notified', 0)
            ->addFieldToFilter('event_date',array('gt' => $dateToFilter))
            ->addFieldToFilter('type_id', $type);
    }

    /*
     * Get mail content from events collection
     *
     * @param $eventsCollection
     * @return string $content
     */
    public function getContentFromEventsCollection($eventsCollection){
        $content = '<h3><b>'.$this->typesArray[$eventsCollection->getFirstItem()->getTypeId()].'</b></h3>';

        foreach($eventsCollection as $event){
            $content .= $this->getMailContentFromEvent($event);
        }

        return $content;
    }

    /*
     * Get email content from an event
     *
     * @param Chernandez_EventsManager_Model_Events $event
     * @return string $content
     */
    public function getMailContentFromEvent(Chernandez_EventsManager_Model_Events $event){

        $mailContent = '<div style="border-top: 1px solid #333; padding-top: 10px;">';
        $mailContent .= '<p>'.$event->getDescription().'<br>';
        $mailContent .= $event->getAdditionalInfo().'<br>';
        $mailContent .= $event->getEventDate().'</p>';
        $mailContent .= '</div>';

        return $mailContent;
    }

}