<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_Repository_Types extends Mage_Core_Model_Abstract
{

    private $typesModel;

    protected function _construct()
    {
        $this->typesModel = Mage::getModel('onestic_eventsmanager/types');
    }

    private function getTypesCollection(){
        return $this->typesModel->getCollection();
    }

    public function getOptionArray()
    {
        $options = array();
        $types = $this->getTypesCollection();

        foreach ($types as $type) {
            $options[$type->getId()] = $type->getLabel();
        }

        return $options;
    }

    public function getTypeOptions()
    {
        $options = '';
        $types = $this->getTypesCollection();

        if(count($types)){
            foreach ($types as $type) {
                $options .= '<option value="' . $type->getId() . '">' . $type->getLabel() . '</option>';
            }
        }

        return $options;
    }


    /*
     * Get Type Id by Label
     *
     * @param string $typeLabel
     * @return int $typeId
     */
    public function getTypeIdByLabel($typeLabel){
        $typeInstance = $this->typesModel->load($typeLabel, 'label');

        if($typeInstance->getId()){
            return $typeInstance->getId();
        } else {
            return 0;
        }
    }

    /*
    * Get Default Type Id
    *
    * @return int $typeId
    */
    public function getDefaultTypeId(){

        return $this->getTypeIdByLabel($this->typesModel->getDefaultLabel());
    }
}