<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_Events extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('onestic_eventsmanager/events');
    }

}