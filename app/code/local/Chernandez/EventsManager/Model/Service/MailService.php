<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_Service_MailService
{
    const DEFAULT_TEMPLATE = 'onestic_eventsmanager_default_template';
    const DEFAULT_SENDER_NAME = 'Chernandez Event Notifier';
    const DEFAULT_RECIPIENT_NAME = 'Event Support';

    private $mailerModel;
    private $emailTemplate;

    public function __construct()
    {
        $this->mailerModel = Mage::getModel('core/email_template');
        $this->emailTemplate = $this->mailerModel->loadDefault(self::DEFAULT_TEMPLATE);
    }

    /*
     * Send Event Email
     *
     * @param string $content
     * @param string $subject
     * @param string $recipients
     */
    public function sendMail($content, $subject, $recipients){

        $emailTemplateVariables = array('event_content' => $content);

        $this->emailTemplate->setSenderName(self::DEFAULT_SENDER_NAME);
        $this->emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
        $this->emailTemplate->setTemplateSubject($subject);
        $this->emailTemplate->setType('html');

        $recipientsEmail = explode(',',trim($recipients));
        foreach($recipientsEmail as $recipientEmail){
            if (!empty($recipientEmail)) {
                $this->emailTemplate->send($recipientEmail, self::DEFAULT_RECIPIENT_NAME, $emailTemplateVariables);
            }
        }

    }

}