<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_Service_EventService extends Mage_Core_Model_Abstract
{

    private $eventModel;

    public function __construct()
    {
        $this->eventModel = Mage::getModel('onestic_eventsmanager/events');
    }

    /*
     * Create an event
     *
     * @param string $description
     * @param string $additional_info
     * @param string $date
     * @param int $type
     * @return int $id
     */
    public function createEvent($description, $additional_info, $date, $type){
        $data = array(
            'description' => $description,
            'additional_info' => $additional_info,
            'event_date' => $date,
            'type_id' => $type
        );

        return $this->saveNewEvent($data);
    }

    /*
     * Create and save an Event
     *
     * @param array $data
     * @return int $id
     */
    private function saveNewEvent(array $data){
        $this->eventModel->setData($data);
        $this->eventModel->save();
        return $this->eventModel->getId();
    }

    /*
     * Update event data
     *
     * @param Chernandez_EventsManager_Model_Events $event
     * @param array $data
     * @return int $id
     */
    public function updateEvent(Chernandez_EventsManager_Model_Events $event, array $data){

        foreach($data as $key => $value){
            $event->setData($key, $value);
        }
        $event->save();

        return $event->getId();
    }

    /*
    * Set events to notified
    *
    * @param $eventsCollection
    */
    public function updateToNotified($eventsCollection){

        $data = array('notified' => 1);

        foreach($eventsCollection as $event){
            $this->updateEvent($event, $data);
        }
    }

}