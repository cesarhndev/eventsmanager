<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_Service_TypeService extends Mage_Core_Model_Abstract
{

    private $typeEventModel;

    public function __construct()
    {
        $this->typeEventModel = Mage::getModel('onestic_eventsmanager/types');
    }

    /*
     * Create and save an Event Type
     *
     * @param array $data
     * @return int $id
     */
    public function createType(array $data){
        $this->typeEventModel->setData($data);
        $this->typeEventModel->save();
        return $this->typeEventModel->getId();
    }

    /*
    * Create type by label
    *
    * @param string $label
    * @return int $id
    */
    public function createTypeByLabel($label){

        if(strlen($label)){
            $data = array('label' => $label);
            return $this->createType($data);
        }
    }

}