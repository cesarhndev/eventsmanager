<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_EventNotifier
{
    private $mailService;

    public function __construct()
    {
        $this->mailService = Mage::getModel('onestic_eventsmanager/service_mailService');
    }

    /*
     * Send email to proper recipient
     *
     * @param string $content
     * @param string $subject
     * @param array $recipients
     */
    public function sendEventNotification($content, $subject, $recipients){
        $this->mailService->sendMail($content, $subject, $recipients);
    }
}