<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_Frequency extends Mage_Core_Model_Abstract
{
    CONST INSTANT_LABEL = 'INSTANT';
    CONST DAILY_LABEL = 'DAILY';
    CONST WEEKLY_LABEL = 'WEEKLY';

    private $frequencyOptions;

    protected function _construct()
    {
        $this->frequencyOptions = array(
            '1' => self::INSTANT_LABEL,
            '2' => self::DAILY_LABEL,
            '3' => self::WEEKLY_LABEL
        );
    }

    /*
     * Return an array of available frequencies
     *
     * @return array
     */
    public function getOptionArray()
    {
        $options = array();

        foreach ( $this->frequencyOptions as $frequencyKey => $frequencyLabel) {
            $options[$frequencyKey] = $frequencyLabel;
        }

        return $options;
    }

    /*
    * Return an option array of available frequencies
    *
    * @return string $options
    */
    public function getFrequencyOptions()
    {
        $options = '';

        foreach ( $this->frequencyOptions as $frequencyKey => $frequencyLabel) {
            $options .= '<option value="' . $frequencyKey . '">' . $frequencyLabel . '</option>';
        }

        return $options;
    }

}