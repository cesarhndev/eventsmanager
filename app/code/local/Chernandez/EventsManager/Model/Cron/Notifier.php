<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Model_Cron_Notifier extends Mage_Core_Model_Abstract
{
    const FREQ_INSTANT = 1;
    const FREQ_DAILY = 2;
    const FREQ_WEEKLY = 3;

    private $eventNotifierModel;
    private $eventRepository;
    private $eventsService;

    public function _construct()
    {
        $this->eventNotifierModel = Mage::getModel('onestic_eventsmanager/eventNotifier');
        $this->eventRepository    = Mage::getModel('onestic_eventsmanager/repository_events');
        $this->eventsService = Mage::getModel('onestic_eventsmanager/service_eventService');
    }

    /*
     * Get config data to notify
     *
     * @param int $frequency
     * @return array $configData
     */
    private function getNotifierConfig($frequency)
    {

        $configData           = array();
        $eventsToNotifyConfig = unserialize(Mage::getStoreConfig('onestic_events/general/notification_settings'));

        foreach ($eventsToNotifyConfig as $eventToNotifyConfig) {

            if ($eventToNotifyConfig['frequency'] == $frequency) {
                $eventData    = array(
                    'type'       => $eventToNotifyConfig['type'],
                    'subject'    => $eventToNotifyConfig['subject'],
                    'recipients' => $eventToNotifyConfig['recipients']
                );
                $configData[] = $eventData;
            }
        }

        return $configData;
    }

    /*
     * Send notification
     *
     * @param int $frequency
     * @param string $dateToFilter
     */
    private function sendNotification($frequency, $dateToFilter)
    {

        $eventsConfigData = $this->getNotifierConfig($frequency);

        if (count($eventsConfigData)) {

            foreach ($eventsConfigData as $eventConfigData) {
                $eventsToNotify = $this->eventRepository->getEventsCollectionTypeToNotify($eventConfigData['type'], $dateToFilter);

                if (count($eventsToNotify)) {
                    $content = $this->eventRepository->getContentFromEventsCollection($eventsToNotify);
                    $this->eventNotifierModel->sendEventNotification($content, $eventConfigData['subject'],
                        $eventConfigData['recipients']);
                    $this->eventsService->updateToNotified($eventsToNotify);
                }
            }
        }
    }

    /*
     * Send an email with events report
     */
    public function instantNotification()
    {
        $dateToFilter = $this->dateToFilter(self::FREQ_INSTANT);
        $this->sendNotification(self::FREQ_INSTANT, $dateToFilter);
    }

    /*
     * Send a daily email with events report
     */
    public function dailyNotification()
    {
        $dateToFilter = $this->dateToFilter(self::FREQ_DAILY);
        $this->sendNotification(self::FREQ_DAILY, $dateToFilter);
    }

    /*
     * Send a weekly email with events report
     */
    public function weeklyNotification()
    {
        $dateToFilter = $this->dateToFilter(self::FREQ_WEEKLY);
        $this->sendNotification(self::FREQ_WEEKLY, $dateToFilter);
    }

    /*
     * Calculate date to filter
     *
     * @param int $frequency_id
     * @return string $dateToFilter
     */
    public function dateToFilter($frequency_id)
    {

        $currentDate = strtotime(Mage::getModel('core/date')->date('Y-m-d H:i:s'));

        switch ($frequency_id){
            case self::FREQ_DAILY:
                $dateToFilter = date('Y-m-d H:i:s', strtotime('-1 days', $currentDate));
                break;
            case self::FREQ_WEEKLY:
                $dateToFilter = date('Y-m-d H:i:s', strtotime('-7 days', $currentDate));
                break;
            default: //FREQ_INSTANT
                $dateToFilter = date('Y-m-d H:i:s', strtotime('-1 hours', $currentDate));
                break;
        }

        return $dateToFilter;
    }
}