<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('onestic_eventsmanager/onestic_event_types')} (
  type_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  label CHAR(40) NOT NULL,
  PRIMARY KEY (type_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('onestic_eventsmanager/onestic_events')} (
  event_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  event_date DATETIME NULL,
  description CHAR(127) NULL,
  additional_info CHAR(127)  NULL,
  type_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (event_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
  ALTER TABLE {$this->getTable('onestic_eventsmanager/onestic_events')} 
    ADD CONSTRAINT `FK_EVENTS_TYPES_TYPES` FOREIGN KEY (`type_id`) 
    REFERENCES {$this->getTable('onestic_eventsmanager/onestic_event_types')}(`type_id`)
      ON UPDATE CASCADE
      ON DELETE CASCADE;
");

$installer->endSetup();