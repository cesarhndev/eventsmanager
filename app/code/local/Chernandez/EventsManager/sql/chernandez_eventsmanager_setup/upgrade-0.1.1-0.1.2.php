<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE {$this->getTable('onestic_eventsmanager/onestic_events')} ADD COLUMN `notified` INT(1) NOT NULL DEFAULT '0';");
$installer->endSetup();
