<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

//Create default type
$typeModel = Mage::getModel('onestic_eventsmanager/types');
$data = array('label' => $typeModel::DEFAULT_TYPE);
$typeModel->setData($data);
$typeModel->save();

