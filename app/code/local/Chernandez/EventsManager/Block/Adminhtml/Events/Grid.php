<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Block_Adminhtml_Events_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();
        $this->setId('onestic_events_grid');
        $this->setDefaultSort('event_id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('onestic_eventsmanager/events')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('event_id', array(
            'header'    => Mage::helper('onestic_eventsmanager')->__('Event ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'event_id',
        ));

        $this->addColumn('event_date', array(
            'header'    => Mage::helper('onestic_eventsmanager')->__('Date registered'),
            'index'     => 'event_date',
        ));

        $this->addColumn('description', array(
            'header'    => Mage::helper('onestic_eventsmanager')->__('Description'),
            'index'     => 'description',
        ));

        $this->addColumn('additional_info', array(
            'header'    => Mage::helper('onestic_eventsmanager')->__('Additional Info'),
            'index'     => 'additional_info',
        ));

        $this->addColumn('type_id', array(
            'header'    => Mage::helper('onestic_eventsmanager')->__('Type'),
            'index'     => 'type_id',
            'type'      => 'options',
            'options'   => Mage::getModel('onestic_eventsmanager/repository_types')->getOptionArray()
        ));

         $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('onestic_eventsmanager/events')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        return $this;
    }

}
