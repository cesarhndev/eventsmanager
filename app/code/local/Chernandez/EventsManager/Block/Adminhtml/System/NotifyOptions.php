<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Block_Adminhtml_System_NotifyOptions extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    public function __construct()
    {
        $this->addColumn('type', array(
                'label' => Mage::helper('onestic_eventsmanager')->__('Event Type'),
                'style' => 'width:80px',
            ));

        $this->addColumn('recipients', array(
                'label' => Mage::helper('onestic_eventsmanager')->__('Recipients to send'),
                'style' => 'width:200px',
            ));

        $this->addColumn('subject', array(
                'label' => Mage::helper('onestic_eventsmanager')->__('Email subject'),
                'style' => 'width:120px',
        ));

        $this->addColumn('frequency', array(
            'label' => Mage::helper('onestic_eventsmanager')->__('Frequency Report'),
            'style' => 'width:80px',
        ));

        $this->_addAfter = true;
        $this->_addButtonLabel = Mage::helper('onestic_eventsmanager')->__('Add');
        parent::__construct();

        $this->setTemplate('onestic/events_manager/system/config/form/field/array.phtml');
    }
    /**
     * @param string $columnName
     *
     * @return null|string
     * @throws Exception
     */
    protected function _renderCellTemplate ($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new Exception('Wrong column name specified.');
        }
        $inputName = $this->getElement()->getName() . '[#{_id}][' . $columnName . ']';

        switch ($columnName) {
            case 'type':
                $rendered = $this->getTypeOptions($inputName);
                break;
            case 'frequency':
                $rendered = $this->getFrequencyOptions($inputName);
                break;
            default:
                $rendered = $this->getInputRender($columnName);
                break;
        }

        return $rendered;
    }


    /**
     * @param $inputName
     *
     * @return string
     */
    protected function getTypeOptions ($inputName)
    {
        $rendered = '<select name="' . $inputName . '" class="required-entry">';
        $rendered .= '<option value="">' . $this->__('-- Select Event Type --') . '</option>';
        $rendered .= Mage::getModel('onestic_eventsmanager/repository_types')->getTypeOptions();
        $rendered .= '</select>';

        return $rendered;
    }

    /**
     * @param $inputName
     *
     * @return string
     */
    protected function getFrequencyOptions ($inputName)
    {
        $rendered = '<select name="' . $inputName . ' " class="select required-entry">';
        $rendered .= Mage::getModel('onestic_eventsmanager/frequency')->getFrequencyOptions();
        $rendered .= '</select>';

        return $rendered;
    }

    /**
     * @param $columnName
     *
     * @return null|string
     * @throws Exception
     */
    protected function getInputRender ($columnName)
    {
        $rendered = null;
        if (empty($this->_columns[$columnName])) {
            throw new Exception('Wrong column name specified.');
        }
        $column    = $this->_columns[$columnName];
        $inputName = $this->getElement()->getName() . '[#{_id}][' . $columnName . ']';
        $rendered  .= '<input type="text" name="' . $inputName . '" value="#{' . $columnName . '}" ' . ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' . (isset($column['class']) ? $column['class'] : 'input-text') . '"' . (isset($column['style']) ? ' style="' . $column['style'] . '"' : '') . '/>';

        return $rendered;
    }
}
