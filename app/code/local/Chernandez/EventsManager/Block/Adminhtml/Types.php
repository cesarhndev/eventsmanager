<?php
/**
 * Chernandez_EventsManager
 *
 * @category   Chernandez
 * @package    Chernandez_EventsManager
 * @copyright  Copyright (c) 2018 Chernandez. (http://www.chernandez.es/)
 */

class Chernandez_EventsManager_Block_Adminhtml_Types extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct()
    {
        $this->_blockGroup      = 'onestic_eventsmanager';
        $this->_controller      = 'adminhtml_types';
        $this->_headerText      = $this->__('Types of events');
        $this->_addButtonLabel  = $this->__('Add New Type of event');
        parent::__construct();
    }

}

